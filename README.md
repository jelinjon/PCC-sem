# Hra Sudoku

## Zadání
Implementujte hru Sudoku.
Hráč vloží vlastní sudoku jako txt soubor, nebo může hrát předpřipravené sudoku, každé jinak náročné.
Následně bude postupně vkládat souřadnice a čísla, která na ně chce vložit.
Program vyhodnotí validnost vstupu a vypíše informaci na termínál. V případě úspěšného vložení vloží číslo na dané pole a překreslí hrací plochu.

## Kompilace programu
Program lze zkompilovat následujícím příkazem:
```bash
cmake -Bcmake-build-debug -H. -DCMAKE_BUILD_TYPE=Debug
cmake --build cmake-build-debug
```

## Spuštění a přepínače
Hra se spouští z příkazové řádky. Nebo z ide.<br>
**Před spuštěním je třeba vložit soubory ze složky Boards v repozitáři do postavené CMake build složky, např. do cmake-build-debug.** <br>

Parametry nastavení programu: <br>
--help (bez dalšího parametru) - vypíše návod na obsluhu programu <br>
--start - slovní reprezentace zvolené náročnosti sudoku, podporované hodnoty jsou "easy", "medium", "hard" <br>
Pokud nejsou parametry programu zadány, načte hra předvyplněné hrací plochu, kde chybí pouze jedno pole, pro ulehčení případného testování konce hry.

Příklady spuštění programu: 
```bash
Sudoku2
Sudoku2 --help
Sudoku2 --start easy
Sudoku2 --start
```

## Ovládání programu
Při zadávání se vypisuje, která souřadnice je aktuálně zadávána.

### Zadávání souřadnic
Souřadnice zadáváme postupně jako tři čísla, v pořadí řádek, sloupec, číslo. Po každém znaku následuje ENTER 

### Ukončení programu
Program je ukončí automaticky, pokud dojde k zaplněné hrací plochy.
Nebo manuálně po uživatelském vstupu q na místo čísla řádky.

## Testování programu

### Příklad testů pro Sudoku
souřadnice a číslo zapsané ve trojici pro přehlednost
#### Pro spuštění programu pomocí --start
-	`q`
-	`000q`
-	`asdq`
-	`003` - uživatel vítězí
#### Pro spuštění programu pomocí --start easy
-	`q`
-	`000q`
-	`003 024 036 048 055 069 087 121 137 164 178 183 219 228 241 266 272 311 322 335 357 368 379 386 408 413 425 449 484 509 526 538 544 551 575 602 631 645 653 667 688 701 773 806 839` - uživatel vítězí

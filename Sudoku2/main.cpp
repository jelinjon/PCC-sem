#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <mutex>
#include <thread>
#include <condition_variable>

#include "window.h"


class sudoku {
public:
    /*
     * Constructor of sudoku class
     */
    explicit sudoku(int dif) {
        board = std::vector<std::vector<int>>(9, std::vector<int>(9, 0));
        boardLC = std::vector<std::vector<bool>>(9, std::vector<bool>(9, false));
        quit = false;
        won = false;
        win = std::make_unique<Window>(std::cout, board);
        difficultyCode = dif;
    }

    ~sudoku() {}

    /*
     * Function to check if a possible board state would be valid
     *
     * Checks for duplicity of to-be-inserted number num in desired row and column,
     * then in the appropriate 3x3 square part of the board in this order
     *
     * returns false, if desired number already exists in any of the checked areas
     */
    bool isValid(int row, int col, int num) {
        for (int x = 0; x <= 8; x++)
            if (board[row][x] == num)
                return false;

        for (int x = 0; x <= 8; x++)
            if (board[x][col] == num)
                return false;

        //calc start row nad col position from desired coords, check 3x3 square from top left to bottom right
        int startRow = row - row % 3, startCol = col - col % 3;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                if (board[i + startRow][j + startCol] == num)
                    return false;

        return true;
    }

    /*
     * checks whether input overwrites the initial puzzle
     * overwriting of inputted data('correcting' mistakes) is allowed
     */
    bool overwritesInitial(int row, int col){
        return boardLC[row][col];
    }

    //checks if win conditions fulfilled
    bool isWin(){
        bool hasZeroes = false;
        for(auto v : board){
            if(std::find(v.begin(), v.end(), 0) != v.end()){
                hasZeroes = true;
            }
        }
        return !hasZeroes;
    }

    //loads board from existing file based on parsed difficulty setting
    //board files need to be inserted manually into cmake-build directory
    void loadBoard(){
        std::ifstream boardFile;

        if(difficultyCode == 0) {
            boardFile.open("easySudoku.txt");
        }
        else if(difficultyCode == 1) {
            boardFile.open("mediumSudoku.txt");
        }
        else if(difficultyCode == 2) {
            boardFile.open("hardSudoku.txt");
        }
        else{
            boardFile.open("debugSudoku.txt");
        }

        if (!boardFile.is_open()) {
            throw std::invalid_argument("File could not be opened");
        }

        //inputs data from file into board vector, and into boardLC vector
        int row = 0, col;
        std::string line;
        while(getline(boardFile, line)){
            col = 0;
            int a;
            std::stringstream ss (line);
            for(int i = 0; i < (line.size()+1)/2; ++i){
                ss >> a;
                board[row][col] = a;

                if (a != 0) {
                    boardLC[row][col] = true;
                }

                col++;
            }

            row++;
        }
    }

    //method for validating integers read from input
    static bool isInteger(const std::string& s){
        for(auto c : s){
            if(!isdigit(c)){
                return false;
            }
        }
        return true;
    }

    //check if row, col and num are within valid range
    static bool isInRange(int row, int col, int num){
        if(row > 8 || row < 0 || col > 8 || col < 0 || num > 9 || num < 1){
            return false;
        }
        return true;
    }


    void input() {
        while (!quit) {
            std::string row, col, num;

            std::unique_lock<std::mutex> lock(mutex);

            //wait until it's input's turn
            inputCV.wait(lock, [this] { return inputTurn; });

            if (!quit) {
                //take player input
                std::cout << "Enter row (0-8), or q to quit: ";
                std::cin >> row;
                if (row == "q") {
                    std::cout << "You chose to quit the game." << std::endl;
                    setQuit(true);
                }
                else {
                    std::cout << "Enter column (0-8): ";
                    std::cin >> col;
                    std::cout << "Enter number (1-9): ";
                    std::cin >> num;

                    //check for ints, continues until ints inputted
                    if (!isInteger(row) || !isInteger(col) || !isInteger(num)) {
                        std::cout << "Invalid input. Please enter a number." << std::endl;
                        continue;
                    }


                    rowI = stoi(row);
                    colI = stoi(col);
                    numI = stoi(num);

                    if(!isInRange(rowI, colI, numI)){
                        std::cout << "Invalid input. Please enter a number from the specified range." << std::endl;
                        continue;
                    }
                }
            }

            //signal calculation thread to proceed
            inputTurn = false;
            calcTurn = true;
            calcCV.notify_one();
        }
    }

    void setQuit(bool bv){
        this->quit = bv;
    }
    void setWon(bool bv){
        this->won = bv;
    }

    void calculation() {
        while (!quit) {

            std::unique_lock<std::mutex> lock(mutex);

            //wait until it's calculation's turn
            calcCV.wait(lock, [this] { return calcTurn; });

            if (!quit) {
                //check for overwrite of init. puzzle, check if valid, input into board
                if (overwritesInitial(rowI, colI)) {
                    std::cout << "Overwriting puzzle forbidden." << std::endl;
                } else if (isValid(rowI, colI, numI)) {
                    board[rowI][colI] = numI;
                } else {
                    std::cout << "Invalid move. Please try again." << std::endl;
                }

                if (isWin()) {
                    setWon(true);
                    setQuit(true);
                }
            }

            //signal output thread to proceed
                calcTurn = false;
                outputTurn = true;
                outputCV.notify_one();

        }
    }

    void output() {
        while (!quit) {

            std::unique_lock<std::mutex> lock(mutex);

            //wait until it's output's turn
            outputCV.wait(lock, [this] { return outputTurn; });

            if(won){
                win->printWin();
            }
            if(quit){
                if (!won) {
                    return;
                }
            }
            else{
                win->repaint();
            }

            //switch priority to input thread
            outputTurn = false;
            inputTurn = true;
            inputCV.notify_one();
        }
    }

    void gameLoop() {
        //start the threads
        std::thread outputThread(&sudoku::output, this);
        std::thread inputThread(&sudoku::input, this);
        std::thread calculationThread(&sudoku::calculation, this);


        //wait for threads to finish
        outputThread.join();
        inputThread.join();
        calculationThread.join();
    }

private:
    std::unique_ptr<Window> win; //window.cpp instance
    std::vector<std::vector<int>> board; //vector of rows of the board
    std::vector<std::vector<bool>> boardLC; //bool signifies if vale in board, at same coords was loaded from file (true) or inputted by user (false)
    bool quit, won;
    int rowI{}, colI{}, numI{};
    int difficultyCode{};

    std::mutex mutex;
    std::condition_variable inputCV;
    std::condition_variable calcCV;
    std::condition_variable outputCV;
    bool inputTurn{false};
    bool calcTurn{false};
    bool outputTurn{true};
};

void showGuide() {
    std::cout << "---------------------------------------------------------" << std::endl;
    std::cout << "Sudoku guide:" << std::endl;
    std::cout << "---------------------------------------------------------" << std::endl;
    std::cout << "Usage: Sudoku.exe [--help] [--start]" << std::endl;
    std::cout << "--help          Shows guide." << std::endl;
    std::cout << "--start + val   Starts the game, loads the corresponding board difficulty (easy/medium/hard) based on value provided." << std::endl;
    std::cout << "                Example usage: Sudoku.exe --start easy" << std::endl;
    std::cout << "\n\r If no value is inputted after --start, the game starts with board missing only one field \n\r the correct solution is [0,0,3], made for faster testing of game end and win detection." << std::endl;
    std::cout << "---------------------------------------------------------" << std::endl;
    std::cout << "Rules: Standard sudoku rules" << std::endl;
    std::cout << "Data entry: 3 integers for row, col, and number to insert, each followed by ENTER key, or q instead of row arg, to quit" << std::endl;
}


int main(int argc, char* argv[]) {
    std::vector<std::string> arguments(argv, argv + argc);

//     Create a modified argv for testing purposes
//    std::vector<std::string> modifiedArguments = {"./program", "--start", "hard"};
//    arguments = modifiedArguments;

    //default difficulty code - debug board
    int argDif = -1;

    //parsing arguments
    for (size_t i = 1; i < arguments.size(); ++i) {
        if (arguments[i] == "--help") {
            showGuide();
            return 0;
        }
        if (arguments[i] == "--start") {
            if(arguments[i+1] == "easy"){argDif = 0;}
            else if(arguments[i+1] == "medium"){argDif = 1;}
            else if(arguments[i+1] == "hard"){argDif = 2;}
            else{argDif = -1;}
        }
    }


    auto s = sudoku(argDif);
    s.loadBoard();
    s.gameLoop();
    return 0;
}
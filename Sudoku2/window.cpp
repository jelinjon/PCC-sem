#include "window.h"

Window::Window(std::ostream &outputStream, std::vector<std::vector<int>> &board) :
        outputStream(outputStream),
        board(board) { }

//method paints the board, with current data, with different separators at every third line
void Window::repaint() {

    for (int i = 0; i < 10; i++) {
        if (i % 3 == 0) {
            outputStream << "------------+-----------+------------" << std::endl;
        }
        else{
            outputStream << "-------------------------------------" << std::endl;
        }
        if(i == 9)break;
        for (int j = 0; j < 9; j++){
            if(j == 0){outputStream << "| ";}
            if(board[i][j] != 0){
                outputStream << board[i][j] << " | ";
            }
            else{
                outputStream << " " << " | ";
            }
        }
        outputStream << std::endl;
    }

}

void Window::printWin() {
    repaint();
    outputStream  << "\n\r   YOU WON   \n";
}

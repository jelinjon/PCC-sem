#ifndef SUDOKU2_WINDOW_H
#define SUDOKU2_WINDOW_H

#include <sstream>
#include <vector>

class Window {
public:
    explicit Window(std::ostream &outputStream, std::vector<std::vector<int>> &board);


    void repaint(); //repaint the board with current state
    void printWin(); //paint state and win message


private:
    std::ostream &outputStream;     // output stream reference (std::cout)
    std::vector<std::vector<int>> &board; // board vector reference
};

#endif //SUDOKU2_WINDOW_H
